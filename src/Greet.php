<?php

namespace Simphiwe\Greet;

class Greet
{
    public function greet(string $sName)
    {
        return 'Hi ' . $sName . '! How are you doing today?';
    }
}
